// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->
// 
// Display one problem at a time
str = intprb_getproblems(1);
No.    file            n    Name
---    ----            -    ----
#  1.  SUM             (10) Sum of X  
expected = [
  "No.    file            n    Name"
  "---    ----            -    ----"
  "#  1.  SUM             (10) Sum of X       "
  ];
expected = stripblanks(expected);
str = stripblanks(str);
assert_checkequal ( str , expected );
//
// With verbose
str = intprb_getproblems(1,%t);
No.    file            n    Name
---    ----            -    ----
#  1.  SUM             (10) Sum of X  
expected = [
  "No.    file            n    Name"
  "---    ----            -    ----"
  "#  1.  SUM             (10) Sum of X       "
  ];
expected = stripblanks(expected);
str = stripblanks(str);
assert_checkequal ( str , expected );
//
// Without verbose
str = intprb_getproblems(1,%f);
expected = [
  "No.    file            n    Name"
  "---    ----            -    ----"
  "#  1.  SUM             (10) Sum of X       "
  ];
expected = stripblanks(expected);
str = stripblanks(str);
assert_checkequal ( str , expected );
//
// Display all problems
nprobmax = intprb_getproblems();
assert_checkequal ( nprobmax > 1 , %t );
for nprob = 1 : nprobmax
  intprb_getproblems(nprob);
  mprintf("\n");
end
No.    file            n    Name
---    ----            -    ----
#  1.  SUM             (10) Sum of X  

No.    file            n    Name
---    ----            -    ----
#  2.  SQSUM           (10) Sum of Squares

No.    file            n    Name
---    ----            -    ----
#  3.  SUMSQROOT       (10) Sum of Square Roots

No.    file            n    Name
---    ----            -    ----
#  4.  PRODONES        (10) Product of Signed Ones

No.    file            n    Name
---    ----            -    ----
#  5.  PRODEXP         (10) Product of Exponentials

No.    file            n    Name
---    ----            -    ----
#  6.  PRODCUB         (10) Product of Cubes

No.    file            n    Name
---    ----            -    ----
#  7.  PRODX           (10) Product of X

No.    file            n    Name
---    ----            -    ----
#  8.  SUMFIFJ         (10) Sum of fi fj

No.    file            n    Name
---    ----            -    ----
#  9.  SUMF1FJ         (10) Sum of f1 fj

No.    file            n    Name
---    ----            -    ----
# 10.  HELLEKALEK      (10) Hellekalek

No.    file            n    Name
---    ----            -    ----
# 11.  ROOSARNOLD1     (10) Roos and Arnold 1

No.    file            n    Name
---    ----            -    ----
# 12.  ROOSARNOLD2     (10) Roos and Arnold 2

No.    file            n    Name
---    ----            -    ----
# 13.  ROOSARNOLD3     (10) Roos and Arnold 3

No.    file            n    Name
---    ----            -    ----
# 14.  RST1            (10) Radovic, Sobol, Tichy (aj=1)

No.    file            n    Name
---    ----            -    ----
# 15.  RST2            (10) Radovic, Sobol, Tichy (aj=j)

No.    file            n    Name
---    ----            -    ----
# 16.  RST3            (10) Radovic, Sobol, Tichy (aj=j^2)

No.    file            n    Name
---    ----            -    ----
# 17.  SOBOLPROD       (10) Sobol Product

No.    file            n    Name
---    ----            -    ----
# 18.  OSCILL          (6)  Genz - Oscillatory

No.    file            n    Name
---    ----            -    ----
# 19.  PRPEAK          (6)  Genz - Product Peak

No.    file            n    Name
---    ----            -    ----
# 20.  CORPEAK         (6)  Genz - Corner Peak

No.    file            n    Name
---    ----            -    ----
# 21.  GAUSSIAN        (6)  Genz - Gaussian

No.    file            n    Name
---    ----            -    ----
# 22.  C0              (6)  Genz - C0 

No.    file            n    Name
---    ----            -    ----
# 23.  DISCONT         (6)  Genz - Discontinuous

// Display all problems in one call
nprobmax = intprb_getproblems();
intprb_getproblems(1:nprobmax);
No.    file            n    Name
---    ----            -    ----
#  1.  SUM             (10) Sum of X  
#  2.  SQSUM           (10) Sum of Squares
#  3.  SUMSQROOT       (10) Sum of Square Roots
#  4.  PRODONES        (10) Product of Signed Ones
#  5.  PRODEXP         (10) Product of Exponentials
#  6.  PRODCUB         (10) Product of Cubes
#  7.  PRODX           (10) Product of X
#  8.  SUMFIFJ         (10) Sum of fi fj
#  9.  SUMF1FJ         (10) Sum of f1 fj
# 10.  HELLEKALEK      (10) Hellekalek
# 11.  ROOSARNOLD1     (10) Roos and Arnold 1
# 12.  ROOSARNOLD2     (10) Roos and Arnold 2
# 13.  ROOSARNOLD3     (10) Roos and Arnold 3
# 14.  RST1            (10) Radovic, Sobol, Tichy (aj=1)
# 15.  RST2            (10) Radovic, Sobol, Tichy (aj=j)
# 16.  RST3            (10) Radovic, Sobol, Tichy (aj=j^2)
# 17.  SOBOLPROD       (10) Sobol Product
# 18.  OSCILL          (6)  Genz - Oscillatory
# 19.  PRPEAK          (6)  Genz - Product Peak
# 20.  CORPEAK         (6)  Genz - Corner Peak
# 21.  GAUSSIAN        (6)  Genz - Gaussian
# 22.  C0              (6)  Genz - C0 
# 23.  DISCONT         (6)  Genz - Discontinuous
//
// Get all problems
nprobmax = intprb_getproblems();
str = intprb_getproblems(1:nprobmax,%f);
expected = [
"No.    file            n    Name"
"---    ----            -    ----"
"#  1.  SUM             (10) Sum of X"
"#  2.  SQSUM           (10) Sum of Squares"
"#  3.  SUMSQROOT       (10) Sum of Square Roots"
"#  4.  PRODONES        (10) Product of Signed Ones"
"#  5.  PRODEXP         (10) Product of Exponentials         "
"#  6.  PRODCUB         (10) Product of Cubes                "
"#  7.  PRODX           (10) Product of X                    "
"#  8.  SUMFIFJ         (10) Sum of fi fj                    "
"#  9.  SUMF1FJ         (10) Sum of f1 fj                    "
"# 10.  HELLEKALEK      (10) Hellekalek                      "
"# 11.  ROOSARNOLD1     (10) Roos and Arnold 1               "
"# 12.  ROOSARNOLD2     (10) Roos and Arnold 2               "
"# 13.  ROOSARNOLD3     (10) Roos and Arnold 3               "
"# 14.  RST1            (10) Radovic, Sobol, Tichy (aj=1)    "
"# 15.  RST2            (10) Radovic, Sobol, Tichy (aj=j)    "
"# 16.  RST3            (10) Radovic, Sobol, Tichy (aj=j^2)  "
"# 17.  SOBOLPROD       (10) Sobol Product                   "
"# 18.  OSCILL          (6)  Genz - Oscillatory"
"# 19.  PRPEAK          (6)  Genz - Product Peak"
"# 20.  CORPEAK         (6)  Genz - Corner Peak"
"# 21.  GAUSSIAN        (6)  Genz - Gaussian"
"# 22.  C0              (6)  Genz - C0"
"# 23.  DISCONT         (6)  Genz - Discontinuous"
];
expected = stripblanks(expected);
str = stripblanks ( str );
assert_checkequal ( str , expected );
