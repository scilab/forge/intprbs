// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


grand("setsd",123456);
nprob = 1;
[n,p] = intprb_getsetup(nprob);
callf = 1.e4;
[ integr , accur , varf ] = intprb_getcrudemc ( nprob , n , p , callf );
assert_checkalmostequal ( integr , 0 , [] , 2.e-1 );
assert_checkequal ( accur < 2.e-1 , %t );
assert_checkalmostequal ( varf , 1 , 1.e-1 );


