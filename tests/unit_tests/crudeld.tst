// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


////////////////////////////////////////

function r = sumfunction (m,n,x)
  // sum of n variables
  // Expectation = 0, Variance = 1
  // Reference
  //   "Computational investigations of low-discrepancy sequences", Kocis,
  //   L. and Whiten, W. J. 1997. ACM Trans. Math. Softw. 23, 2 (Jun. 1997),
  //   266-294. 
  //   This is function F1.

    // The function value
    // The expectation of f (before scaling)
    v = n/12
    // The variance of f (before scaling)
    e = n/2
    r = sqrt(1/v) * (sum(x,"c") - e)
endfunction
//
//
//////////////////////////////////////////
//
// Test with default settings
dim_num = 10;
integr = intprb_crudeld ( sumfunction , dim_num );
assert_checkalmostequal ( integr , 0 , [], 1.e-1 );
//
// Customize ldseq
dim_num = 10;
//ldseq = "haltonf";
ldseq = "halton";
integr = intprb_crudeld ( sumfunction , dim_num , ldseq );
assert_checkalmostequal ( integr , 0 , [], 1.e-1 );
//
// Customize callf
dim_num = 10;
ldseq = [];
callf = 1.e4;
integr = intprb_crudeld ( sumfunction , dim_num , ldseq , callf );
assert_checkalmostequal ( integr , 0 , [], 1.e-2 );
//
// Test with customized bounds
// integral_10^30 sqrt(x)  = 88.46266043324404
// E(f) = integral_10^30 sqrt(x)  * 1/20 = 4.423133021662201
// V(f) = integral_10^30 (sqrt(x) - 4.423133021662201 )^2 * 1/20 = 0.4358942726814049
function r = sqrtfunction (m,n,x)
  r = sqrt(x)
endfunction
dim_num = 1;
callf = [];
ldseq = [];
bounds = [10 30];
integr = intprb_crudeld ( sqrtfunction , dim_num , ldseq , callf , bounds );
assert_checkalmostequal ( integr , 88.462660433244027 , 1.e-6 );

//
// Customize the function :
// use a function which has additionnal parameters.
function r = sumfunction2 (m,n,x,e,v)
    r = sqrt(1/v) * (sum(x,"c") - e)
endfunction
dim_num = 3;
func = list(sumfunction2,dim_num/2,dim_num/12);
integr = intprb_crudeld ( sumfunction , dim_num );
assert_checkalmostequal ( integr , 0 , [], 1.e-3 );
//
// Try all possible sequences
dim_num = 10;
callf = 10;
for ldseq = lowdisc_methods()'
  mprintf("Checking sequence %s\n",ldseq)
  integr = intprb_crudeld ( sumfunction , dim_num , ldseq , callf );
end

// Customize ldseq with a home-made Halton sequence 
// restricted to dimension 10.
function result = vdc ( i , basis )
  current = i
  ib = 1.0 / basis
  result = 0.0
  while (current>0)
    digit = modulo ( current , basis )
    current = int ( current / basis )
    result = result + digit * ib
    ib = ib / basis
  end
endfunction
function next = haltonsequence ( m , n )
  next = zeros ( m , n )
  primematrix = [2,3,5,7,11,13,17,19,23,29]
  for k = 1 : m
    for i = 1 : n
      basis = primematrix ( i )
      next(k,i) = vdc ( k , basis )
    end
  end
endfunction
dim_num = 3;
callf = 100;
integr = intprb_crudeld ( sumfunction , dim_num , haltonsequence , callf );
assert_checkalmostequal ( integr , 0 , [], 1.e-1 );

//
// Customize ldseq, which has additionnal parameters
function next = haltonsequence2 ( m , n , skip , leap , prmat )
  next = zeros ( m , n )
  k = skip + 1
  index = 0
  for index = 1 : m
    for i = 1 : n
      basis = prmat ( i )
      next(index,i) = vdc ( k , basis )
    end
    k = k + leap
  end
endfunction
dim_num = 3;
primematrix = [2,3,5,7,11,13,17,19,23,29];
skip = 10;
// Kocis and Whiten recommend leap = 409.
leap = 409;
ldseq = list(haltonsequence2,skip,leap,primematrix);
callf = 100;
integr = intprb_crudeld ( sumfunction , dim_num , ldseq , callf );
assert_checkalmostequal ( integr , 0 , [], 1.e-1 );

// Customize the function :
// use a function which has additionnal parameters.
function r = sumfunction2 (m,n,x,e,v)
r = sqrt(1/v) * (sum(x,"c") - e)
endfunction
n = 3;
func = list(sumfunction2,n/2,n/12);
integr = intprb_crudeld ( func , n );
assert_checkalmostequal ( integr , 0 , [], 1.e-1 );


