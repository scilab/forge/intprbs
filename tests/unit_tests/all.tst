// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



////////////////////////////////////////

function checkProblem ( nprob ) 
  
  mprintf("Testing problem #%d\n",nprob)
  //
  mprintf("  intprb_getname...\n")
  name=intprb_getname(nprob)
  assert_checkequal ( typeof(name) , "string" )
  assert_checkequal ( size(name) , [1 1] )
  mprintf("  name=%s\n",name)
  //
  mprintf("  intprb_getsetup...\n")
  [n,p]=intprb_getsetup(nprob)
  assert_checkequal ( size(n) , [1 1] )
  assert_checkequal ( typeof(n) , "constant" )
  assert_checkequal ( typeof(p) , "constant" )
  //
  mprintf("  intprb_getfunc, option=1 (function value)...\n")
  option = 1
  m = 10
  x = grand(m,n,"def")
  f=intprb_getfunc(m,n,x,p,nprob,option)
  assert_checkequal ( typeof(f) , "constant" )
  assert_checkequal ( size(f) , [m 1] )
  //
  mprintf("  intprb_getfunc, option=2 (expectation)...\n")
  option = 2
  e=intprb_getfunc([],n,[],p,nprob,option)
  assert_checkequal ( typeof(e) , "constant" )
  if ( e <> [] ) then
    assert_checkequal ( size(e) , [1 1] )
  end
  //
  mprintf("  intprb_getfunc, option=3 (variance)...\n")
  option = 3
  var=intprb_getfunc([],n,[],p,nprob,option)
  assert_checkequal ( typeof(var) , "constant" )
  if ( var <> [] ) then 
    assert_checkequal ( size(var) , [1 1] )
  end
  //
  mprintf("  intprb_getfunc, option=4 (number of dimensions)...\n")
  option = 4
  n_default=intprb_getfunc([],[],[],[],nprob,option)
  assert_checkequal ( typeof(n_default) , "constant" )
  assert_checkequal ( size(n_default) , [1 1] )
  //
  mprintf("  intprb_getfunc, option=5 (full name)...\n")
  option = 5
  name=intprb_getfunc([],[],[],[],nprob,option)
  assert_checkequal ( typeof(name) , "string" )
  assert_checkequal ( size(name) , [1 1] )
  //
  mprintf("  intprb_getfunc, option=6 (variable dimension)...\n")
  option = 6
  vardim=intprb_getfunc([],[],[],[],nprob,option)
  assert_checkequal ( typeof(vardim) , "boolean" )
  assert_checkequal ( size(vardim) , [1 1] )
  //
  mprintf("  intprb_getfunc, option=7 (parameters)...\n")
  option = 7
  p=intprb_getfunc([],n,[],[],nprob,option)
  assert_checkequal ( typeof(p) , "constant" )
  //
  mprintf("  intprb_getfcn...\n")
  m = 10
  x = grand(m,n,"def")
  f=intprb_getfcn(m,n,x,p,nprob);
  assert_checkequal ( typeof(f) , "constant" )
  assert_checkequal ( size(f) , [m 1] );
  //
  mprintf("  intprb_getexpect...\n")
  m = 10
  x = grand(m,n,"def")
  e=intprb_getexpect(n,p,nprob);
  assert_checkequal ( typeof(e) , "constant" )
  if ( e <> [] ) then
    assert_checkequal ( size(e) , [1 1] );
  end
  //
  mprintf("  intprb_getvariance...\n")
  m = 10
  x = grand(m,n,"def")
  v=intprb_getvariance(n,p,nprob);
  assert_checkequal ( typeof(v) , "constant" )
  if ( v <> [] ) then 
    assert_checkequal ( size(v) , [1 1] );
  end
endfunction

// Get the number of problems
nprobmax = intprb_getproblems();
//
// Check all tests
for nprob = 1 : nprobmax
  checkProblem ( nprob ) 
end



