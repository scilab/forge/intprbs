// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


x = 1 : 10
[ n , mu , var ] = intprb_muvstepstart ( );
for i = 1 : 10
  [ n , mu , var ] = intprb_muvstepupdate ( n , mu , var , x(i) );
end
[ n , mu , var ] = intprb_muvstepstop ( n , mu , var );
assert_checkalmostequal ( mean(x) , mu , %eps );
assert_checkalmostequal ( variance(x) , var , %eps );

