<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from intprb_getfcn.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="intprb_getfcn" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>intprb_getfcn</refname>
    <refpurpose>Returns the function value.</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   f=intprb_getfcn(m,n,x,p,nprob)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>m:</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, the number of experiments</para></listitem></varlistentry>
   <varlistentry><term>n:</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, the dimension of the space</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para> a m-by-n matrix of doubles, the point where to compute f.</para></listitem></varlistentry>
   <varlistentry><term>p:</term>
      <listitem><para> a matrix of doubles, the parameters of the problem</para></listitem></varlistentry>
   <varlistentry><term>nprob:</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, the problem number</para></listitem></varlistentry>
   <varlistentry><term>f:</term>
      <listitem><para> a m-by-1 matrix of doubles, the function value</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
This function computes the function value at the point <literal>x</literal>.
   </para>
   <para>
Internally, it is an interface function which calls the function
<literal>getfunc</literal>, which selects
appropriate test fuction based on <literal>nprob</literal>.
   </para>
   <para>
See the "Integration Problems User's Manual" available in the doc
directory of this toolbox for a detailed description of this function.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Get function value at [1 1 1] for Sum test case
nprob = 1
[n,p] = intprb_getsetup(nprob)
x = ones(1,n)
m = 1
f = intprb_getfcn(m,n,x,p,nprob)

// Get function value for randomized experiments for Sum test case
nprob = 1
[n,p] = intprb_getsetup(nprob)
m = 10
x = grand(m,n,"def")
f = intprb_getfcn(m,n,x,p,nprob)
// Print summary statistics
[min(f) max(f) mean(f) stdev(f)]

// Perform a Monte-Carlo on Sum
nprob = 1;
[n,p] = intprb_getsetup(nprob)
e = intprb_getexpect(n,p,nprob)
v = intprb_getvariance(n,p,nprob)
for m = logspace(3,5,3)
x = grand(m,n,"def");
f = intprb_getfcn(m,n,x,p,nprob);
meanm = mean(f);
varm = variance(f);
mprintf("%10d %+7.5f %.7f %.7f\n",m,meanm,varm,varm/m);
end

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011,2013 - Michael Baudin</member>
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Integration Problems User's Manual", Michael Baudin, 2010</para>
</refsection>
</refentry>
