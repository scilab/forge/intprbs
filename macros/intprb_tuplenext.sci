// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2007 - John Burkardt
//
// This file must be used under the terms of the GNU LGPL license.

function [ rnk, x ] = intprb_tuplenext ( m1, m2, n, rnk, x )
  // Computes the next element of a tuple space.
  //
  // Calling Sequence
  //   [ rnk, x ] = intprb_tuplenext ( m1, m2, n, rnk, x )
  //
  //  Parameters
  //    m1: a 1-by-1 matrix of doubles, integer value, the minimum entry.
  //    m2: a 1-by-1 matrix of doubles, integer value, the maximum entry.
  //    n: a 1-by-1 matrix of doubles, integer value, positive, the number of components.
  //    rnk: a 1-by-1 matrix of doubles, integer value, positive, the index of the tuple. On input, the index of the previous tuple. On output, the index of the next tuple. 
  //    x: a n-by-1 matrix of doubles, the tuple. On input, the previous tuple. On output, the next tuple.
  //
  // Description
  //    This function produces the vector x, with n components, 
  //    which have integer components in the range [m1,m1+1,...,m2].
  //    The elements are n vectors.  Each entry is constrained to lie
  //    between m1 and m2.  The elements are produced one at a time.
  //    The first element is
  //
  //      <screen>
  //      (m1,m1,...,m1),
  //      </screen>
  //
  //    the second element is
  //
  //      <screen>
  //      (m1,m1,...,m1+1),
  //      </screen>
  //
  //    and the last element is
  //
  //      <screen>
  //      (m2,m2,...,m2).
  //      </screen>
  //
  //    Intermediate elements are produced in lexicographic order.
  //
  //    On first call, set the input argument <literal>rnk</literal> to 0.  
  //    On subsequent calls, the input value of <literal>rnk</literal> should 
  //    be the output value of <literal>rnk</literal> from the previous call. 
  //    When there are no more elements, <literal>rnk</literal> will be returned as 0.
  //
  //    This function is used in the <literal>intprb_corpeak</literal> function.
  //
  //  Examples
  //    // Displays all tuples of length 5 with elements between 0 and 1.
  // x = [];
  // rnk = 0;
  // // Tuple #1:
  // [ rnk, x ] = intprb_tuplenext ( 0, 1, 5, rnk, x )
  // // Tuple #2:
  // [ rnk, x ] = intprb_tuplenext ( 0, 1, 5, rnk, x )
  // // Tuple #3:
  // [ rnk, x ] = intprb_tuplenext ( 0, 1, 5, rnk, x )
  // // etc...
  //
  //    // Displays all tuples of length 2 with elements between 1 and 3.
  //    n = 2;
  //    m1 = 1;
  //    m2 = 3;
  //    rnk = 0;
  //    x = [];
  //    computed = [];
  //    for i = 1 : 12
  //      [ rnk, x ] = intprb_tuplenext ( m1, m2, n, rnk, x );
  //      disp([rnk x'])
  //    end
  //    // Notice that the vector [0 0 0] announces that the end has been reached.
  //    // From there, the algorithm circles.
  //
  // Authors
  //  2007 - John Burkardt (MATLAB version)
  //  2010 - Michael Baudin (Scilab version)
  
  // No checking for performance reasons
  
  if ( m2 < m1 ) then
    rnk = 0
    x = []
    return
  end
  if ( rnk <= 0 ) then
    x(1:n) = m1
    rnk = 1
  else
    rnk = rnk + 1
    i = n
    while ( %t )
      if ( x(i) < m2 ) then
        x(i) = x(i) + 1
        break
      end
      x(i) = m1
      if ( i == 1 ) then
        rnk = 0
        x(1:n) = 0
        break
      end
      i = i - 1
    end
  end
endfunction

