// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function intprb_contour ( nprob )
  // Plots a 2D contour of the function.
  //
  // Calling Sequence
  //   intprb_contour ( nprob )
  //
  // Parameters
  //   nprob: a 1-by-1 matrix of matrix of floating point integers, the problem number
  //
  // Description
  // Uses the contour function to plot the 2D contour of the test function.
  //
  // Examples
  //   // Plot the contour of the SUM  function
  //   scf();
  //   mprintf("Please wait...\n")
  //   intprb_contour ( 1 )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "intprb_contour" , rhs , 1 )
  apifun_checklhs ( "intprb_contour" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype   ( "intprb_contour" , nprob ,    "nprob" ,    1 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "intprb_contour" , nprob ,    "nprob" ,    1 )
  //
  // Check content
  nprobmax = intprb_getproblems()
  apifun_checkoption ( "intprb_contour" , nprob ,    "nprob" ,    1 , 1 : nprobmax )
  //
  body = [
  "x = [x1 x2]"
  "[n,p]=intprb_getsetup("+string(nprob)+")"
  "n = 2"
  "m = size(x,""r"")"
  "y = intprb_getfcn(m,n,x,p,"+string(nprob)+");"
  ];
  prot=funcprot();
  funcprot(0);
  deff("y=funForContour(x1,x2)",body);
  funcprot(prot);
  //
  nx = 50
  x1 = linspace ( 0 , 1 , nx )
  x2 = linspace ( 0 , 1 , nx )
  nlevels = 10
  contour(x1,x2,funForContour,nlevels)
  h = gcf()
  h.children.data_bounds = [
    0    0.
    1    1.
    ]
endfunction

