// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.



function [ integr , accur , varf ] = intprb_getcrudemc ( nprob , n , p , callf )
  //  Uses a crude Monte-Carlo to estimate the integral.
  //
  // Calling Sequence
  //   [ integr , accur , varf ] = intprb_getcrudemc ( nprob , n , p , callf )
  //
  //  Parameters
  //   nprob: a 1-by-1 matrix of doubles, integer value,the problem number
  //   n: a 1-by-1 matrix of doubles, integer value, the spatial dimension.
  //   p: a matrix of doubles, the parameters of the problem
  //   callf : a 1-by-1 matrix of floating point integers, the number of calls to the function (default = 1.e4)
  //   integr : a 1-by-1 matrix of doubles, the approximate value of the integral, the mean of the function.
  //   accur : a 1-by-1 matrix of doubles, the estimated error on the integral.
  //   varf : a 1-by-1 matrix of doubles, the approximate value of the variance of f.
  // 
  // Description
  //   Uses a crude Monte-Carlo to approximate the integral corresponding 
  //   to the nprob problem.
  //   This function is a wrapper on intprb_crudemc.
  //
  // Examples
  // // Consider problem #1
  // nprob = 1;
  // [n,p] = intprb_getsetup(nprob);
  // callf = 1.e4;
  // // Get the exact result
  // e = intprb_getexpect(n,p,nprob)
  // // Use a Monte Carlo
  // [integr,accur,varf]=intprb_getcrudemc(nprob,n,p,callf)
  //
  // Authors
  //  2010 - Michael Baudin (Scilab version)
  //

  //
  // Check input arguments
  apifun_checktype ( "intprb_getcrudemc" , nprob ,   "nprob" ,   1 , "constant" )
  apifun_checktype ( "intprb_getcrudemc" , n ,       "n" ,       2 , "constant" )
  apifun_checktype ( "intprb_getcrudemc" , p ,       "p" ,       3 , "constant" )
  apifun_checktype ( "intprb_getcrudemc" , callf ,   "callf" ,   4 , "constant" )
  //
  apifun_checkscalar ( "intprb_getcrudemc" , nprob ,   "nprob" ,   1 )
  apifun_checkscalar ( "intprb_getcrudemc" , n ,       "n" ,       2 )
  // Do not check p : let the test functions manage it as they want.
  apifun_checkscalar ( "intprb_getcrudemc" , callf ,   "callf" ,   4 )
  //
  // Define a wrapper over the intprb function
  body = [
    "y = intprb_getfcn ( callf , n , x , p , nprob );"
  ];
  prot=funcprot();
  funcprot(0);
  deff("y = func ( callf , n , x , p , nprob )",body);
  funcprot(prot);
  //
  [ integr , accur , varf ] = intprb_crudemc ( func , n , callf )
endfunction

