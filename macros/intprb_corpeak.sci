// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2007 - John Burkardt
// Copyright (C) 1987 - Alan Genz
//
// This file must be used under the terms of the GNU LGPL license.

function r = intprb_corpeak (m,n,x,p,option)
  
  // Corner Peak function by Genz
  // A little modification was done.
  // In Genz's testpack, the integration is done over [a,b], with a=0
  // and b = alpha.
  // Here, the integration is done over [0,1]^n, so that multiplying  
  // x by alpha is necessary to get the same problem.
  // Reference
  //    Alan Genz,
  //    A Package for Testing Multiple Integration Subroutines,
  //    in Numerical Integration:
  //    Recent Developments, Software and Applications,
  //    edited by Patrick Keast, Graeme Fairweather,
  //    D Reidel, 1987, pages 337-340,
  //    LC: QA299.3.N38.
  
  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_corpeak",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    p_alpha = p(1:n)
    p_beta = p(n+1:2*n)
    //
    //  For this case, the BETA's are used to randomly select
    //  a corner for the peak.
    //
    x = x .* (ones(m,1) * p_alpha')
    iblow = find(p_beta < 0.5)
    terms = ones(m,1) * (p_alpha') - x
    terms(1:m,iblow) = x(1:m,iblow)
    total = 1 + sum(terms,"c")
    r = total.^( -n - 1 )
    // 
    // Compute the expectation to scale the function
    e = 0.0
    sgndm = 1/gamma(n+1)
    if ( modulo(n,2) == 1 ) then
      sgndm = -sgndm
    end
    rnk = 0
    ic = []
    while ( %t )
      [ rnk, ic ] = intprb_tuplenext ( 0, 1, n, rnk, ic )
      if ( rnk == 0 )
        break
      end
      total = 1.0 + sum(p_alpha(find(ic <> 1)))
      isum = sum ( ic )
      s = 1 + 2 * ( floor ( isum / 2 ) * 2 - isum )
      e = e + s / total
    end
    e = e * sgndm
    // Scale f to make it between the [a,b] bounds
    // Here, a = 0, b = alpha
    e = e ./ prod(p_alpha)
    // Scale the function
    r = r - e
  case 2 then
    // The expectation
    p_alpha = p(1:n)
    p_beta = p(n+1:2*n)
    r = 0
  case 3 then
    // The variance
    r = []
  case 4
    // The default number of variables
    r = 6
  case 5
    // The full name of the test case
    r = "Genz - Corner Peak"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    //p_alpha = grand(n,1,"def") would be faster, but not as close to the original source code
    seed = 123456
    p_alpha = zeros(n,1)
    p_beta = zeros(n,1)
    for i = 1 : n
      [ value, seed ] = intprb_shragerandom ( seed )
      p_alpha(i) = value
      [ value, seed ] = intprb_shragerandom ( seed )
      p_beta(i) = value
    end
    // Scale a as recommended in Genz in testpack.f : s^2/600
    suma = sum(p_alpha)
    scale = n^2 / 600
    p_alpha = p_alpha / suma / scale
    r(1:n) = p_alpha
    r(n+1:2*n) = p_beta
  end
endfunction

