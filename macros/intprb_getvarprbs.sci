// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function prb_n = intprb_getvarprbs ()
  // Returns the array of problems with variable n.
  //
  // Calling Sequence
  //   prb_n = intprb_getvarprbs()
  //
  // Parameters
  //   prb_n: a 1-by-1 matrix of doubles, integer value, the problems which have a variable n
  //
  // Description
  // This function returns the array of problems with variable n.
  // For these problems, the user can set the size of the 
  // problem to an arbitrary value n.
  //
  // See the "Integration Problems User's Manual" available in the doc 
  // directory of this toolbox for a detailed description of this function.
  //
  // Examples
  //   // Get the matrix of variable size problems
  //   prbmat = intprb_getvarprbs()
  //
  // Authors
  // Copyright (C) 2011 - Michael Baudin
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //
  // Bibliography
  //   "Integration Problems User's Manual", Michael Baudin, 2010
  
  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_getvarprbs" , rhs , 0 )
  apifun_checklhs ( "intprb_getvarprbs" , lhs , 0 : 1 )
  //
  nprobmax = intprb_getproblems()
  prb_n = []
  for nprob = 1 : nprobmax
    vardim = intprb_getfunc([],[],[],[],nprob,6)
    if ( vardim ) then
      prb_n ($+1) = nprob
    end
  end
endfunction

