// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function e = intprb_getexpect (n,p,nprob)
  // Returns the expectation of the problem.
  //
  // Calling Sequence
  //   e = intprb_getexpect (n,p,nprob)
  //
  // Parameters
  //   n: a 1-by-1 matrix of doubles, integer value, the number of variables, i.e. the size of x
  //   p: a matrix of doubles, the parameters of the problem
  //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
  //   e: a 1-by-1 matrix of doubles, the expectation of the problem
  //
  // Description
  // It is an interface function which calls the function getfunc (which selects  
  // appropriate test function based on nprob).
  // Currently, all functions are designed to have a zero integral.
  //
  // See the "Integration Problems User's Manual" available in the doc 
  // directory of this toolbox for a detailed description of this function.
  //
  // Examples
  //   // Get expectation for the Sum test case
  //   nprob = 1
  //   [n,p] = intprb_getsetup(nprob)
  //   e=intprb_getexpect(n,p,nprob)
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (C) 2011 - Michael Baudin
  //
  // Bibliography
  //   "Integration Problems User's Manual", Michael Baudin, 2010
  
  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_getexpect" , rhs , 3 )
  apifun_checklhs ( "intprb_getexpect" , lhs , 0 : 1 )
  //
  apifun_checktype   ( "intprb_getexpect" , n ,       "n" ,      1 , "constant" )
  apifun_checkscalar ( "intprb_getexpect" , n ,       "n" ,      1 )
  apifun_checktype   ( "intprb_getexpect" , p ,       "p" ,      2 , "constant" )
  // Do not check the size of p : let the function manage it.
  apifun_checktype   ( "intprb_getexpect" , nprob ,   "nprob" ,  3 , "constant" )
  apifun_checkscalar ( "intprb_getexpect" , nprob ,   "nprob" ,  3 )
  //
  x = []
  m = []
  e = intprb_getfunc(m,n,x,p,nprob,2)
endfunction

