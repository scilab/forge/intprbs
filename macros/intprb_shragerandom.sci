// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2007 - John Burkardt
// Copyright (C) 1979 - Linus Schrage
//
// This file must be used under the terms of the GNU LGPL license.

function [ value , seed ] = intprb_shragerandom ( seed )
  //  A portable random number generator by Schrage.
  //
  // Calling Sequence
  //   [ value, seed ] = intprb_shragerandom ( seed )
  //
  //  Parameters
  //    seed : a 1-by-1 matrix of doubles, integer value, a seed for the random number generator. The seed must be a positive, nonzero floating point integer less than 2147483647. 
  //    value : 1-by-1 matrix of doubles, a pseudorandom value uniform in [0,1].
  // 
  // Description
  //   This is a portable random number generator by Schrage.
  //   The generator is based on the multiplicative congruential
  //   generator 
  //   
  //   <screen>
  //   X = A * X (mod M) 
  //   </screen>
  //   
  //   with
  //   
  //   <screen>
  //   A = 16807
  //   </screen>
  //   
  //   and 
  //   
  //   <screen>
  //   M = 2^31 - 1 = 2147483647.
  //   </screen>
  //   
  //   Schrage states that the generator is full cycle, with period M.
  //   The implementation may be slower than Park and Miller's "Good ones are hard to find".
  //   Genz uses the seed = 123456.
  //
  //   This function is used in Genz's problem, for the computation 
  //   of the parameters of the function.
  //
  // Examples
  // // A sequence of random numbers uniform in [0,1]
  // seed = 123456;
  // [ value, seed ] = intprb_shragerandom ( seed )
  // [ value, seed ] = intprb_shragerandom ( seed )
  // [ value, seed ] = intprb_shragerandom ( seed )
  //
  // // Another sequence
  // seed = 1
  // for i = 1 : 1000
  //   [ value, seed ] = intprb_shragerandom ( seed );
  // end
  // // Must be : seed = 522329230
  // mprintf("seed = %d",seed)
  //
  // Authors
  // Copyright (C) 2011 - Michael Baudin
  // Copyright (C) 2010 - DIGITEO - Michael Baudin (Scilab version)
  // Copyright (C) 2007 - John Burkardt (MATLAB version)
  // Copyright (C) 1979 - Linus Schrage
  //
  // Bibliography
  //    "A More Portable Fortran Random Number Generator", Linus Schrage, ACM Transactions on Mathematical Software, Volume 5, Number 2, June 1979, pages 132-138.
  //    "A pseudo-random number generator for the system/360", Lewis, P A W, Goodman, A.S., And Miller, J M . IBM Syst. 3". 8, 2 (1969), 136-146.
  //

  a = 16807
  b15 = 32768
  b16 = 65536
  p = 2147483647
  
  xhi = floor ( seed / b16 )
  xalo = ( seed - xhi * b16 ) * a
  leftlo = floor ( xalo / b16 )
  fhi = xhi * a + leftlo
  k = floor ( fhi / b15 )
  seed = ( ( ( xalo - leftlo * b16 ) - p  ) + ( fhi - k * b15 ) * b16 ) + k
  if ( seed < 0 )
    seed = seed + p
  end
  value = seed / p
endfunction

