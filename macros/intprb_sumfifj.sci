// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r = intprb_sumfifj (m,n,x,p,option)

  // Sum of fi fj, where fj is piecewise constant
  // Reference
  //   "Computational investigations of low-discrepancy sequences", Kocis,
  //   L. and Whiten, W. J. 1997. ACM Trans. Math. Softw. 23, 2 (Jun. 1997),
  //   266-294. 
  //   This is function F8.

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_sumfifj",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    s = sqrt(2/n/(n-1))
    f = zeros(m,n)
    k1 = find ( x<1/6 | x>4/6 )
    f(k1) = 1
    k2 = find ( x>1/6 & x<4/6 )
    f(k2) = -1
    //
    c = cumsum(f,"c")
    c = [zeros(m,1) c(:,1:n-1)]
    g = f .* c
    //
    r = s * sum(g,"c")
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Sum of fi fj"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction

