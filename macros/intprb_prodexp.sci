// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





function r = intprb_prodexp (m,n,x,p,option)

  // Product of fj, where fj depends on EXP.
  // Reference
  //   "Computational investigations of low-discrepancy sequences", Kocis,
  //   L. and Whiten, W. J. 1997. ACM Trans. Math. Softw. 23, 2 (Jun. 1997),
  //   266-294. 
  //   This is function F5.

  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_prodexp",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    s = sqrt((15*exp(15)+15)/(13*exp(15)+17))
    exp30x = exp(30*x-15)
    f = (exp30x-1)./(exp30x+1)
    r = s^n * prod(f,"c")
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = 1
  case 4
    // The default number of variables
    r = 10
  case 5
    // The full name of the test case
    r = "Product of Exponentials"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    r = []
  end
endfunction

