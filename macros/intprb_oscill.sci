// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2007 - John Burkardt
// Copyright (C) 1987 - Alan Genz
//
// This file must be used under the terms of the GNU LGPL license.

function r = intprb_oscill (m,n,x,p,option)
  
  // Oscillatory function by Genz.
  // A little modification was done.
  // In Genz's testpack, the integration is done over [a,b], with a=0
  // and b = alpha.
  // Here, the integration is done over [0,1]^n, so that multiplying  
  // x by alpha is necessary to get the same problem.
  // Reference
  //    Alan Genz,
  //    A Package for Testing Multiple Integration Subroutines,
  //    in Numerical Integration:
  //    Recent Developments, Software and Applications,
  //    edited by Patrick Keast, Graeme Fairweather,
  //    D Reidel, 1987, pages 337-340,
  //    LC: QA299.3.N38.
  
  if ( option<1 | option>7 ) then
    msg = msprintf(gettext("%s: Error: The option value is %d, while either %d to %d are expected.") , "intprb_oscill",option,1,7);
    error ( msg )
  end
  //
  select option
  case 1 then
    // The function value
    p_alpha = p(1:n)
    p_beta = p(n+1:2*n)
    //
    // The expectation
    e = 2.0^n * ..
      cos ( 2.0 * %pi * p_beta(1) + 0.5 * sum ( p_alpha ) ) * ..
      prod ( sin ( 0.5 * p_alpha ) ./ p_alpha );
    //
    arg = 2.0 * %pi * p_beta(1) + x * p_alpha
    r = cos ( arg ) - e
  case 2 then
    // The expectation
    r = 0
  case 3 then
    // The variance
    r = []
  case 4
    // The default number of variables
    r = 6
  case 5
    // The full name of the test case
    r = "Genz - Oscillatory"
  case 6
    // Set true if the dimension is variable
    r = %t
  case 7
    // The parameters
    //p_alpha = grand(n,1,"def") would be faster, but not as close to the original source code
    seed = 123456
    p_alpha = zeros(n,1)
    p_beta = zeros(n,1)
    for i = 1 : n
      [ value, seed ] = intprb_shragerandom ( seed )
      p_alpha(i) = value
      [ value, seed ] = intprb_shragerandom ( seed )
      p_beta(i) = value
    end
    // Scale a as recommended in Genz in testpack.f : s^1.5/110
    suma = sum(p_alpha)
    scale = n^(3/2) / 110
    p_alpha = p_alpha / suma / scale
    r(1:n) = p_alpha
    r(n+1:2*n) = p_beta
  end
endfunction

