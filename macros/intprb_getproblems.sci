// Copyright (C) 2011 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function result = intprb_getproblems (varargin)
  // Lists the problems.
  //
  // Calling Sequence
  //   nprobmax = intprb_getproblems()
  //   str = intprb_getproblems(prbmat)
  //   str = intprb_getproblems(prbmat,verbose)
  //
  // Parameters
  //   nprobmax: a 1-by-1 matrix of doubles, integer value, the maximum problem number. Valid problem numbers are then produced by 1:nprobmax.
  //   prbmat: a matrix of doubles, integer value, the indices of the problem numbers
  //   verbose: a 1-by-1 matrix of booleans, if true then prints the problem description (default = true).
  //   str: a 1-by-1 matrix of strings, a description of the problem
  //
  // Description
  // This function lists the respective information associated with the test   
  // function number in <literal>prbmat</literal>. 
  // There is a direct map from the problem 
  // name to the function name. For example, the "SUM" problem corresponds to the 
  // <literal>intprb_sum</literal> function which is in the 
  // <literal>intprb_sum.sci</literal> file. 
  // The numbers in parenthesis (n) means that the files include 
  // parameters for varying the dimension of the problem.
  //
  // Examples
  //   // Display one problem at a time
  //   intprb_getproblems(1);
  //   intprb_getproblems(3);
  //
  //   // Display all problems
  //   nprobmax = intprb_getproblems()
  //   intprb_getproblems(1:nprobmax);
  //
  //   // Get all problems
  //   nprobmax = intprb_getproblems()
  //   str = intprb_getproblems(1:nprobmax,%f)
  //
  //   // Do a Monte-Carlo on all problems
  //   stacksize("max");
  //   nprobmax = intprb_getproblems();
  //   for nprob = 1 : nprobmax
  //     shortname = intprb_getname(nprob);
  //     longname = intprb_getname(nprob,%t);
  //     mprintf("=======================\n");
  //     mprintf("Problem #%d : %s (%s)\n",nprob,longname,shortname);
  //     [n,p]=intprb_getsetup(nprob);
  //     e = intprb_getexpect(n,p,nprob);
  //     v = intprb_getvariance(n,p,nprob);
  //     for m = logspace(3,5,3)
  //       x = grand(m,n,"def");
  //       f = intprb_getfcn(m,n,x,p,nprob);
  //       meanm = mean(f);
  //       varm = variance(f);
  //       mprintf("%10d %+7.5f %.7f %.7f\n",m,meanm,varm,varm/m);
  //       clear("f");
  //       clear("x");
  //     end
  //   end
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (C) 2011 - Michael Baudin

  
  [lhs, rhs] = argn();
  apifun_checkrhs ( "intprb_getproblems" , rhs , 0 : 2 )
  apifun_checklhs ( "intprb_getproblems" , lhs , 0 : 1 )
  //
  namematrix = intprb_getname()
  //
  if (rhs == 0) then
    // The maximum number of problems available
    result = size ( namematrix , "*" )
    return;
  end
  //
  prbmat = varargin(1)
  if ( rhs < 2 ) then
    verbose = %t
  else
    verbose = varargin(2)
  end
  //
  // Check input arguments
  apifun_checktype   ( "intprb_getproblems" , prbmat ,    "prbmat" ,    1 , "constant" )
  nbp = size(prbmat,"*")
  apifun_checkvecrow ( "intprb_getname" ,     prbmat ,     "prbmat" ,   1 , nbp );
  apifun_checktype   ( "intprb_getproblems" , verbose ,    "verbose" ,  2 , "boolean" )
  apifun_checkscalar ( "intprb_getproblems" , verbose ,    "verbose" ,  2 )
  //
  str(1) = "No.    file            n    Name"
  str(2) = "---    ----            -    ----"
  k = 3
  //
  // Make the report
  for nprob = prbmat
    n = intprb_getfunc([],[],[],[],nprob,4)
    sname = intprb_getname ( nprob )
    uname = convstr(sname,"u")
    lname = intprb_getfunc([],[],[],[],nprob,5)
    isvarying = intprb_getfunc([],[],[],[],nprob,6)
    if ( isvarying ) then
      str_n = "(" + string(n) + ")"
    else
      str_n = string(n)
    end
    str(k) = msprintf("# %2d.  %-15s %-4s %-10s", nprob,uname,str_n,lname)
    k = k + 1
  end
  if ( verbose ) then
    for i = 1 : k-1
      mprintf("%s\n",str(i))
    end
  end
  result = str
  // 
endfunction


