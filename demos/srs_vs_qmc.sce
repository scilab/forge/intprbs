// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


nprob=17; // Index of the integration problem
emax=18; // Maximum power of 2 for the number of points
nrepeat=10; // Number of repetitions of the experiment
mprintf("Test problem #%d\n",nprob);
mprintf("Maximum number of points up to 2**%d\n",emax);
mprintf("Number of repetitions : %d\n",nrepeat);
//
npoints=2^(1:emax); // Number of points in the sample
//
shortname = intprb_getname(nprob);
longname = intprb_getname(nprob,%t);
// n : dimension of the space
// p : a vector of extra parameters
[n,p] = intprb_getsetup(nprob);
e = intprb_getexpect(n,p,nprob);
h=scf();
strtitle=msprintf("Problem #%d - Convergence of approximate integral.",nprob)
xtitle(strtitle);
xlabel("Number of Points");
ylabel("Absolute Error");
plot(npoints,1 ./sqrt(npoints),"r-");
plot(npoints,1 ./npoints,"b-");
first=%t;
for callf = npoints
    // SRS
    abserrSRS=[];
    for i = 1:nrepeat
        integr=intprb_getcrudemc(nprob,n,p,callf);
        abserrSRS($+1)=abs(integr-e);
    end
    plot(callf*ones(nrepeat,1),abserrSRS,"ro-");
    // Low Discrepancy
    lds = lowdisc_new("halton");
    lds = lowdisc_configure(lds,"-dimension",n);
    // Avoid to consider the the first block of callf points
    // (including zero), because zero is a part of the (t,m,s)-net, 
    // but we don't want to evaluate f at zero.
    lds = lowdisc_configure(lds,"-skip",callf-1);
    abserrLDS=[];
    for i = 1:nrepeat
        [lds,x] = lowdisc_next (lds,callf);
        y = intprb_getfcn ( callf , n , x , p , nprob );
        integr = mean(y);
        abserrLDS($+1)=abs(integr-e);
    end
    lds = lowdisc_destroy(lds);
    plot(callf*ones(nrepeat,1),abserrLDS,"bo-");
    //
    if (first) then
        legend(["$1/\sqrt{n}$","$1/n$","Monte-Carlo","Halton"]);
        h.children.log_flags="lln";
        first=%f;
    end
end
