// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function demo_scrambledSobol()

    function u = sobolScrambled(m,n,scrambling)
        // Returns a scrambled Sobol sequence
        //
        // Parameters
        // m : number of points in the sequence
        // n : number of dimensions
        lds = lowdisc_new("sobol");
        lds = lowdisc_configure(lds,"-dimension",n);
        seeds=distfun_unifrnd(0,1,1,24);
        lds = lowdisc_configure(lds,"-seeds",seeds);
        lds = lowdisc_configure(lds,"-scrambling",scrambling);
        [lds,u] = lowdisc_next (lds,m);
        lds = lowdisc_destroy(lds);
    endfunction

    //
    // The test provided in Algo 823 
    // for the scrambled Sobol sequence.
    // "Algorithm 823: Implementing scrambled digital sequences", 
    // Hee Sun Hong, Fred J. Hickernell, 
    // ACM, Transactions on mathematical software, 
    // Volume 29, NO. 2, June, 2003, P. 95--109.
    //
   
    n = 2; // Number of dimensions
    disp('m = Number of points')
    disp('EI = Estimated integral')
    nprob=12; // ROOSARNOLD2
    SAMS=20; // SAMS : Number of replications
    //scrambling = "Owen";
    // scrambling = "Faure-Tezuka";
    scrambling = "Owen-Faure-Tezuka";
    maxtime=0.5; // Maximum time for one simulation
    //
    h=scf();
    xtitle("Non Scrambled vs ScrambledSobol")
    xlabel("Number of Points")
    ylabel("Absolute Error")
    //
    p=[]; // Extra-parameters for the function evaluation.
    e = intprb_getexpect(n,p,nprob); // Exact integral
    stopthere=%f;
    for m = 2.^(3:30)
        //
        // Without Scrambling
        // Get blocks of m points in the sequence.
        lds = lowdisc_new("sobol");
        lds = lowdisc_configure(lds,"-dimension",n);
        abserror=[];
        for II = 1:SAMS
            tic();
            [lds,x] = lowdisc_next (lds,m);
            f=intprb_getfcn(m,n,x,p,nprob);
            t=toc();
            if (t>maxtime) then
                stopthere=%t;
            end
            EI=sum(f)/m;
            mprintf('m = %d, EI = %e.7 (time=%.2f (s))\n',m,EI,t);
            abserror($+1)=abs(e-EI);
        end
        plot(m*ones(SAMS,1),abserror,"ro-")
        lds = lowdisc_destroy(lds);
        //
        // With Scrambling
        abserror=[];
        for II = 1:SAMS
            tic();
            x = sobolScrambled(m,n,scrambling);
            f=intprb_getfcn(m,n,x,p,nprob);
            t=toc();
            if (t>maxtime) then
                stopthere=%t;
            end
            EI=sum(f)/m;
            mprintf('m = %d, EI = %e.7 (time=%.2f (s))\n',m,EI,t);
            abserror($+1)=abs(e-EI);
        end
        plot(m*ones(SAMS,1),abserror,"bo-")
        h.children.log_flags="lln";
        legend(["No Scrambling",scrambling]);
        if (stopthere) then
            break
        end
    end

    filename = 'bench_scrambled-sobol.sce';
    demo_viewCode(filename);
endfunction 
demo_scrambledSobol();
clear demo_scrambledSobol


