// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function demo_shiftedhalton()
    function u = shiftedHalton(m,n)
        // Returns m points of a shifted Halton sequence, 
        // using a single random uniform in [0,1]**n.
        // 
        // Parameters
        // m : number of points
        // n : dimension of the space
        lds = lowdisc_new("halton");
        lds = lowdisc_configure(lds,"-dimension",n);
        [lds,u] = lowdisc_next (lds,m);
        lds = lowdisc_destroy(lds);
        x=grand(1,n,"def"); // Random shift
        u=modulo(x(ones(m,1),:)+u,1);// RQMC
    endfunction

    //
    function [integr, accur] = RQMCHalton(nprob, nmc, callf)
        // Estimates the integral with a Shifted Halton 
        // sequence, based on the RQMC method.
        // 
        // Parameters
        // nprob : the index of the problem
        // nmc : the number of independent Monte-Carlo experiments
        // callf : the number of points in each sample
        [n,p] = intprb_getsetup(nprob);
        for i=1:nmc
            x = shiftedHalton(callf,n)
            f=intprb_getfcn(callf,n,x,p,nprob);
            integrArray(i)=mean(f)
        end
        integr = mean(integrArray); // Estimate of the integral
        std = stdev(integrArray); // Standard variation
        // The probability is 95% that the exact integral is 
        // in the range [integr-2*accur,integr+2*accur]. 
        accur = std / sqrt(callf - 1);
    endfunction
    //
    // Estimates the integral and its accuracy 
    // with a RQMC method.
    nmc=32; // Number of repetitions
    nprob=17; // Index of problem
    maxtime=2.; // Maximum time for one simulation
    nmc=32; // Number of repetitions
    nprob=17; // Index of problem
    nexperiments=10; // Number of experiments
    //
    // Check convergence when m increases
    p=[]; // Extra parameters for function evaluation
    e = intprb_getexpect(n,p,nprob); // Exact integral
    //
    h=scf();
    xtitle("Convergence of Shifted-Halton")
    xlabel("Number of Points")
    ylabel("Absolute Error")
    stopthere=%f;
    for callf= 2.^(3:30)
        abserror=[];
        for i=1:nexperiments
            tic();
            [integr, accur] = RQMCHalton(nprob, nmc, callf);
            t=toc();
            abserror($+1)=abs(e-integr);
            mprintf("N=%d, Error=%.7e, Time=%.2f (s)\n",callf,abs(e-integr),t)
        end
        plot(callf*ones(nexperiments,1),abserror,"bo-")
        h.children.log_flags="lln";
        if (t>maxtime) then
            break
        end
    end

    filename = 'bench_shifted-halton.sce';
    demo_viewCode(filename);
endfunction 
demo_shiftedhalton();
clear demo_shiftedhalton


