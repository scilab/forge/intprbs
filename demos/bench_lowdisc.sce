// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


function ldprob ( nprob )
  shortname = intprb_getname(nprob);
  longname = intprb_getname(nprob,%t);
  mprintf("=======================\n");
  mprintf("Problem #%d : %s (%s)\n",nprob,longname,shortname);
  [ n , p ] = intprb_getsetup(nprob);
  mprintf("Dimension : %s\n",string(n));
  e = intprb_getexpect(n,p,nprob);
  var = intprb_getvariance(n,p,nprob);
  if ( var == [] ) then
    mprintf("Expectation = %+10.5e\n",e);
  else
    mprintf("Expectation = %+10.5e, Variance =%10.7e\n",e,var);
  end
  mprintf("%10s %10s\n","m","Mean");
  for callf = logspace(1,4,4)
    integr = intprb_getcrudeld ( nprob , n , p , callf )
    mprintf("%10d %+10.5e\n",callf,integr);
  end
endfunction

//
//
// Do A Quasi Monte-Carlo on all problems

nprobmax = intprb_getproblems();
for nprob = 1 : nprobmax
  ldprob ( nprob )
end


//
// Load this script into the editor
//
filename = "bench_lowdisc.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

