Integration Test Problems Toolbox

Purpose
-------

The goal of this toolbox is to provide integration test problems
in order to test integration algorithms.
This allows to test integration algorithms on a larger base of 
functions.
Developping these functions is error-prone, so that using this 
module allows to save much time.
Moreover, testing an algorithm leads to the difficulty 
of chosing a set of test functions.
But all functions are not equally difficult to integrate; 
while some are simple, other might be particularily difficult,
leading to poor results. 
Comparing two algorithms, each on a different set of functions 
will not lead to an accurate view of the algorithm.
This module allows to compare the algorithms more precisely.

In spirit, this module has the same motivations as the Unconstrained 
Optimization Scilab toolbox based on the More, Garbow and Hillstrom 
collection of functions.

A manual, available in pdf, is provided in the doc directory
of this module.
It provides a detailed analysis of each function and references
where each function is used or analysed.

Overview
--------

The module currently provides 23 functions :

No.    file            n    Name
---    ----            -    ----
#  1.  SUM             (10) Sum of X  
#  2.  SQSUM           (10) Sum of Squares
#  3.  SUMSQROOT       (10) Sum of Square Roots
#  4.  PRODONES        (10) Product of Signed Ones
#  5.  PRODEXP         (10) Product of Exponentials
#  6.  PRODCUB         (10) Product of Cubes
#  7.  PRODX           (10) Product of X
#  8.  SUMFIFJ         (10) Sum of fi fj
#  9.  SUMF1FJ         (10) Sum of f1 fj
# 10.  HELLEKALEK      (10) Hellekalek
# 11.  ROOSARNOLD1     (10) Roos and Arnold 1
# 12.  ROOSARNOLD2     (10) Roos and Arnold 2
# 13.  ROOSARNOLD3     (10) Roos and Arnold 3
# 14.  RST1            (10) Radovic, Sobol, Tichy (aj=1)
# 15.  RST2            (10) Radovic, Sobol, Tichy (aj=j)
# 16.  RST3            (10) Radovic, Sobol, Tichy (aj=j^2)
# 17.  SOBOLPROD       (10) Sobol Product
# 18.  OSCILL          (6)  Genz - Oscillatory
# 19.  PRPEAK          (6)  Genz - Product Peak
# 20.  CORPEAK         (6)  Genz - Corner Peak
# 21.  GAUSSIAN        (6)  Genz - Gaussian
# 22.  C0              (6)  Genz - C0 
# 23.  DISCONT         (6)  Genz - Discontinuous

Features
---------

The following is a list of functions of the module.

 * intprb_getexpect : Returns the expectation of the problem.
 * intprb_getfcn : Returns the function value.
 * intprb_getname : Returns the name.
 * intprb_getproblems : Lists the problems.
 * intprb_getsetup : Returns the parameters of the problem.
 * intprb_getvariance : Compute the variance.
 * intprb_getvarprbs : Returns the array of problems with variable n.
 * Integration Algorithms
   * intprb_crudeld : Estimates a multidimensional integral using a Low Discrepancy sequence.
   * intprb_crudemc : Estimates a multidimensional integral using Monte Carlo.
   * intprb_getcrudeld : Uses a crude Quasi Monte-Carlo to estimate the integral.
   * intprb_getcrudemc : Uses a crude Monte-Carlo to estimate the integral.
   * intprb_montecarlo : Estimates a multidimensional integral using iterated Monte Carlo.
 * Support Functions
   * intprb_contour : Plots a 2D contour of the function.
   * intprb_muvblockupdate : Update the mean and the variance of x, given the mean and the value of disjoint parts of x.
   * intprb_muvstepstart : Returns the starting values for the step-by-step (mean,variance) computation.
   * intprb_muvstepstop : Finish the computation of the step-by-step (mean,variance) computation.
   * intprb_muvstepupdate : Update values for the step-by-step (mean,variance) computation.
   * intprb_shragerandom : A portable random number generator by Schrage.
   * intprb_tuplenext : Computes the next element of a tuple space.

Dependencies
------------

 * This module depends on the apifun module (>= v0.2).
 * This module depends on the helptbx module (to build the help pages).
 * This module depends on the assert module.
 * This module depends on the lowdisc module (>= v0.2).

Demos
-----

 * genzphi : Estimates the normal cumulative density function
 * bench_mc : Test intprb_getcrudemc on all problems
 * bench_lowdisc : Test intprb_getcrudeld on all problems

Adding One Test Function
------------------------

In order to add one new test function, we must perform the 
following modifications :
 * create a intprb_mynewf() function
 * update intprb_getname() and add "mynewf" at the end of namematrix 



 
TODO: 
-------
 * update intprb_contour to customize the number of points into account
 * update intprb_contour to create the title and the x and y labels
 * update intprb_contour to customize the number of levels
 * gather more problems
 * create a routine to give the estimated number of iterations 
  This may work for smooth functions. For discontinuous functions, 
  for example where the function is nonzero only for a small region,
  the estimate may be be completely wrong.
 * add RST4, with ai=(s-i+1)^2 as in "Generalized Halton Sequences in 2007 : a Comparative Study", Faure, Lemieux, 2007.
 * add SOBASOS, as a product of 1+c(xi-0.5) where (c,s) are chosen as (c=0.1,s=120), (c=0.25,s=96), (c=1,s=150) as in 
  "Generalized Halton Sequences in 2007 : a Comparative Study", Faure, Lemieux, 2007 
   and "One more experiment on estimating high dimensional integrals by quasi Monte Carlo methods", Sobol, Asostsky, 
   Math. Comput. Simul., 62, 2003, 255-263

References
----------

 * "Integration Test Problems User's Manual", Michael Baudin, 2010.
 * "Computational investigations of low-discrepancy sequences", Kocis, L. and Whiten, W. J. 1997. ACM Trans. Math. Softw. 23, 2 (Jun. 1997), 266-294.
 * "Quadrature Rule Applied to Test Integrals", http://people.sc.fsu.edu/~jburkardt/m_src/quadrature_test/quadrature_test.html, John Burkardt, 2007
 * "Testing Multidimensional Integration Routines", http://people.sc.fsu.edu/~jburkardt/m_src/testpack/testpack.html, John Burkardt, 2007
 * "Multi-dimensional Integration Test Functions", http://people.sc.fsu.edu/~jburkardt/m_src/test_nint/test_nint.html, John Burkardt, 2007
 * "Testpack", http://www.math.wsu.edu/faculty/genz/software/fort77/testpack.f, Alan Genz, 1987

ATOMS
-----

http://atoms.scilab.org/toolboxes/intprbs

Forge
-----

http://forge.scilab.org/index.php/p/intprbs/

Authors
-------

 * Copyright (C) 2011 - Michael Baudin
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
 * Copyright (C) 2007 - John Burkardt
 * Copyright (C) 1987 - Alan Genz

Licence
-------

This toolbox is released under the GNU LGPL license.


